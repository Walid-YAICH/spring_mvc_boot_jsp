package tn.esprit.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {

	@RequestMapping("/sayHello")
	public String welcome(Map<String, Object> model,
						  @RequestParam("myName") String name) {
		model.put("receivedName", name);
		return "helloPage";
	}

}