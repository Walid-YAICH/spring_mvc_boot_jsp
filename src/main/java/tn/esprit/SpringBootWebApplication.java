package tn.esprit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 
 * @author Walid YAICH
 *
 * See application.properties
 * http://localhost:8080/SpringMVC/servlet/sayHello?myName=Walid
 * 
 * 
 */
@SpringBootApplication
public class SpringBootWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebApplication.class, args);
	}

}